import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  describe('default screen', () => {
    beforeEach(() => {
      page.navigateTo('/Info Health');
    });
    it('should say Info Health', () => {
      expect(page.getParagraphText()).toContain('Info Health');
    });
  });
});
