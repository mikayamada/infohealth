import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MariliaPage } from './marilia.page';

const routes: Routes = [
  {
    path: '',
    component: MariliaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MariliaPageRoutingModule {}
