import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MariliaPage } from './marilia.page';

describe('MariliaPage', () => {
  let component: MariliaPage;
  let fixture: ComponentFixture<MariliaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MariliaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MariliaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
