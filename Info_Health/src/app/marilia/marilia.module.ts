import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MariliaPageRoutingModule } from './marilia-routing.module';

import { MariliaPage } from './marilia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MariliaPageRoutingModule
  ],
  declarations: [MariliaPage]
})
export class MariliaPageModule {}
