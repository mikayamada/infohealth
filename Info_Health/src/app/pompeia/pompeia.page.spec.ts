import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PompeiaPage } from './pompeia.page';

describe('PompeiaPage', () => {
  let component: PompeiaPage;
  let fixture: ComponentFixture<PompeiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PompeiaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PompeiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
