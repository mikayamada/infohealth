import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PompeiaPageRoutingModule } from './pompeia-routing.module';

import { PompeiaPage } from './pompeia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PompeiaPageRoutingModule
  ],
  declarations: [PompeiaPage]
})
export class PompeiaPageModule {}
