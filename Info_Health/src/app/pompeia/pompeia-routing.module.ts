import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PompeiaPage } from './pompeia.page';

const routes: Routes = [
  {
    path: '',
    component: PompeiaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PompeiaPageRoutingModule {}
