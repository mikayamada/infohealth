import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrientePage } from './oriente.page';

const routes: Routes = [
  {
    path: '',
    component: OrientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrientePageRoutingModule {}
