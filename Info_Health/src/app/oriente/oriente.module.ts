import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrientePageRoutingModule } from './oriente-routing.module';

import { OrientePage } from './oriente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrientePageRoutingModule
  ],
  declarations: [OrientePage]
})
export class OrientePageModule {}
