import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrientePage } from './oriente.page';

describe('OrientePage', () => {
  let component: OrientePage;
  let fixture: ComponentFixture<OrientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
