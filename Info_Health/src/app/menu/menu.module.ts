import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'dicas',
        loadChildren: () => import('../dicas/dicas.module').then( m => m.DicasPageModule)
      },
      {
        path: 'marilia',
        loadChildren: () => import('../marilia/marilia.module').then( m => m.MariliaPageModule)
      },
      {
        path: 'oriente',
        loadChildren: () => import('../oriente/oriente.module').then( m => m.OrientePageModule)
      },
      {
        path: 'pompeia',
        loadChildren: () => import('../pompeia/pompeia.module').then( m => m.PompeiaPageModule)
      },
      {
        path: 'tupa',
        loadChildren: () => import('../tupa/tupa.module').then( m => m.TupaPageModule)
      },
      {
        path: 'sobre',
        loadChildren: () => import('../sobre/sobre.module').then( m => m.SobrePageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/menu/home'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
