import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterEvent } from '@angular/router';
import { Router } from '@angular/router'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
    {
      title: 'Home',
      url: '/menu/home'
    },
    {
      title: 'Dicas',
      url: '/menu/dicas'
    },
    {
      title: 'Marília',
      url: '/menu/marilia'
    },
    {
      title: 'Oriente',
      url: '/menu/oriente'
    },
    {
      title: 'Pompéia',
      url: '/menu/pompeia'
    },
    {
      title: 'Tupã',
      url: '/menu/tupa'
    },
    {
      title: 'Sobre',
      url: '/menu/sobre'
    },
  ];

  selectedPath = '';

  constructor(private router: Router) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
  }

}
