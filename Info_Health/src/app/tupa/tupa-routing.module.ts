import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TupaPage } from './tupa.page';

const routes: Routes = [
  {
    path: '',
    component: TupaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TupaPageRoutingModule {}
