import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TupaPage } from './tupa.page';

describe('TupaPage', () => {
  let component: TupaPage;
  let fixture: ComponentFixture<TupaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TupaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TupaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
