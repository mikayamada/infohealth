import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TupaPageRoutingModule } from './tupa-routing.module';

import { TupaPage } from './tupa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TupaPageRoutingModule
  ],
  declarations: [TupaPage]
})
export class TupaPageModule {}
